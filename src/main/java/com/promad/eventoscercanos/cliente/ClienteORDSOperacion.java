package com.promad.eventoscercanos.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.promad.eventoscercanos.dto.ParametrosDataDto;



@FeignClient(value = "MS-DATA-CQ", url="http://3.14.155.2:8980")
public interface ClienteORDSOperacion {

	@RequestMapping(method = RequestMethod.POST, value = "/operacionData/")
    ResponseEntity<String> operacionDatos(@RequestBody ParametrosDataDto parametrosDataDto, @RequestHeader("jsonDatos") String jsonDatos);

	
}
