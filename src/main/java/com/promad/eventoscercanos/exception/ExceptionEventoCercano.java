package com.promad.eventoscercanos.exception;

public class ExceptionEventoCercano extends RuntimeException {

	private static final long serialVersionUID = 1L;

    private String codigo;
    
    public ExceptionEventoCercano() {
    	super();
    }
    
    public ExceptionEventoCercano(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionEventoCercano(String message, String codigo) {
        super(message);
        this.codigo = codigo;
    }
	
    public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

    public ExceptionEventoCercano(Throwable throwable) {
        super(throwable);
    } 

}
