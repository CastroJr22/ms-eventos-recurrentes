package com.promad.eventoscercanos.dto;

import java.util.List;


public class ParametrosDataDto {
	
    String nombrePaquete;
    String nombreStoreProcedure;
    String nombreMS;
    String tipo = "";
    List<ParametroDto> param;

    public ParametrosDataDto() {

    }
    
    public String getNombrePaquete() {
        return nombrePaquete;
    }

    public void setNombrePaquete(String nombrePaquete) {
        this.nombrePaquete = nombrePaquete;
    }

    public String getNombreStoreProcedure() {
        return nombreStoreProcedure;
    }

    public void setNombreStoreProcedure(String nombreStoreProcedure) {
        this.nombreStoreProcedure = nombreStoreProcedure;
    }

    public String getNombreMS() {
        return nombreMS;
    }

    public void setNombreMS(String nombreMS) {
        this.nombreMS = nombreMS;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<ParametroDto> getParam() {
        return param;
    }

    public void setParam(List<ParametroDto> param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return "ParametrosDataDto [nombrePaquete=" + nombrePaquete + ", nombreStoreProcedure=" + nombreStoreProcedure + "NombreMs=" + getNombreMS()
                + ", param=" + param + ", getNombrePaquete()=" + getNombrePaquete() + ", getNombreStoreProcedure()="
                + getNombreStoreProcedure() + ", getParam()=" + getParam().toString() + ", getClass()=" + getClass()
                + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }

}
