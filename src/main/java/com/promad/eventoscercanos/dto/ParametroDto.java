package com.promad.eventoscercanos.dto;

public class ParametroDto {
	
	private String nombreParametro;
    private String tipo;
    private Object valor;

    public ParametroDto(String nombreParametro, String tipo, Object valor) {
        this.nombreParametro = nombreParametro;
        this.tipo = tipo;
        this.valor = valor;
    }

    public ParametroDto() {

    }
    
	public String getNombreParametro() {
		return nombreParametro;
	}

	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}
	
	@Override
    public String toString() {
        return "ParametrosDto [nombreParametro=" + nombreParametro + ", tipo=" + tipo + ", valor=" + valor + "]";
    }
    
}
