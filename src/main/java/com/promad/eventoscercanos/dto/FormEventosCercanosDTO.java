package com.promad.eventoscercanos.dto;

public class FormEventosCercanosDTO {
	
	private String uuid;
	private String latitud;
	private String longitud;
	private String vSalida;
		
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getvSalida() {
		return vSalida;
	}
	public void setvSalida(String vSalida) {
		this.vSalida = vSalida;
	}

}
