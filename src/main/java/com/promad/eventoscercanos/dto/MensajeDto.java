package com.promad.eventoscercanos.dto;

public class MensajeDto {
	private String mensaje;
    private String estatus;
    private String codigo;

    public MensajeDto() {

    }
	
    public MensajeDto(String mensaje, String estatus, String codigo){

        this.mensaje = mensaje;
        this.estatus = estatus;
        this.codigo = codigo;

    }
	
    public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
    
}
