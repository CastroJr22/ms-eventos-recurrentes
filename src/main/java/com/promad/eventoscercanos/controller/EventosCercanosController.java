package com.promad.eventoscercanos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.promad.eventoscercanos.dto.FormEventosCercanosDTO;
import com.promad.eventoscercanos.dto.MensajeDto;
import com.promad.eventoscercanos.exception.ExceptionEventoCercano;
import com.promad.eventoscercanos.service.EventosCercanosService;

@CrossOrigin()
@RestController
@RequestMapping("api/eventoscercanos")
public class EventosCercanosController {

	@Autowired
	EventosCercanosService eventosCercanosService;
	
	@PostMapping("/getEventos")
    public ResponseEntity<?> getEventoCercano(@RequestBody FormEventosCercanosDTO jsonFormEventosCercanos) {

        if(jsonFormEventosCercanos.getUuid()== null || jsonFormEventosCercanos.getUuid().isEmpty()) {

            return new ResponseEntity<MensajeDto>(new MensajeDto("UUID inv�lido", "Failed", "001"), HttpStatus.BAD_REQUEST);

        }
        try {
        	
            String json = eventosCercanosService.getEventoCercano(jsonFormEventosCercanos.getUuid(), jsonFormEventosCercanos.getLatitud(), jsonFormEventosCercanos.getLongitud(), jsonFormEventosCercanos.getvSalida());

            return new ResponseEntity<String>(json, HttpStatus.OK);
            
        }catch (ExceptionEventoCercano e) {
            
        	return new ResponseEntity<MensajeDto>(new MensajeDto("" + e.getMessage(), "Failed", e.getCodigo()), HttpStatus.BAD_REQUEST);
        
        }
    }
	
}
