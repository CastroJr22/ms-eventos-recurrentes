package com.promad.eventoscercanos.service;

public interface EventosCercanosService {
	
	public String getEventoCercano(String uuid, String latitud,  String longitud, String vSalida);   
    
}
