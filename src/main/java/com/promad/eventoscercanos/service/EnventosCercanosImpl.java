package com.promad.eventoscercanos.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.eventoscercanos.dto.ParametroDto;
import com.promad.eventoscercanos.dto.ParametrosDataDto;
import com.promad.eventoscercanos.exception.ExceptionEventoCercano;
import com.promad.eventoscercanos.cliente.ClienteORDSInformacion;
// import com.promad.eventoscercanos.cliente.ClienteORDSOperacion;

@Service
@PropertySource("classpath:application.properties")
public class EnventosCercanosImpl implements EventosCercanosService {

    @Value("telefonista")
    private String telefonista;
    @Value("evento_cercano")
    private String eventoCercano;
    @Value("MS-EVENTOS-CERCANOS")
    private String nombreMS;

    
    @Autowired
    ClienteORDSInformacion clienteORDSInformacion;
	
    @Override
	public String getEventoCercano(String uuid, String latitud, String longitud, String vSalida) {
		String json = null;
		
        try {
            ParametrosDataDto parametrosData = new ParametrosDataDto();
            parametrosData.setNombreMS(nombreMS);
            parametrosData.setNombrePaquete(telefonista);
            parametrosData.setNombreStoreProcedure(eventoCercano);

            List<ParametroDto> listParametro = new ArrayList<>();
            listParametro.add(getParametro("uuid", "String", uuid));
            listParametro.add(getParametro("latitud", "String", latitud));
            listParametro.add(getParametro("longitud", "String", longitud));
            listParametro.add(getParametro("v_salida", "String", vSalida));
            parametrosData.setParam(listParametro);

                json = clienteORDSInformacion.consultarInformacion(parametrosData, new Gson().toJson(parametrosData)).getBody();
        }
        catch (Exception e) {

            throw new ExceptionEventoCercano("Error al comunicarse con servicio MS_Q", "004");
        }
		
		return json;
	}

    private ParametroDto getParametro(String nombreParametro, String tipo, Object valor) {
        ParametroDto parametro = new ParametroDto(nombreParametro, tipo, valor);

        return parametro;
    }

	
}
